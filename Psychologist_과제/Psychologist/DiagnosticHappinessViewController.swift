//
//  DiagnosticHappinessViewController.swift
//  Psychologist
//
//  Created by Jo Sung Uk on 2016. 5. 1..
//  Copyright © 2016년 Jo Sung Uk. All rights reserved.
//

import UIKit

class DiagnosticHappinessViewController: HappinessViewController, UIPopoverPresentationControllerDelegate{
    override var happiness: Int {
        didSet {
            diagnosticHistory += [ happiness ]
        }
    }
    
    private let defaults = NSUserDefaults.standardUserDefaults()
    
    var diagnosticHistory: [ Int ] {
        get{ return defaults.objectForKey(History.DefaultsKey) as? [ Int ] ?? [ ] }
        set{ defaults.setObject(newValue, forKey:History.DefaultsKey) }
    }
    
    private struct History {
        static let SegueIdentifier = "Show Diagnostic History"
        static let DefaultsKey = "DiagnosticHappinessViewController.History"
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if let identifier = segue.identifier {
            switch identifier {
            case History.SegueIdentifier:
                if let vc = segue.destinationViewController as? TextViewController {
                    if let ppc = vc.popoverPresentationController {
                        ppc.delegate = self
                    }
                    vc.text = "\(diagnosticHistory)"
                }
            default: break
            }
        }
    }
    
    func adaptivePresentationStyleForPresentationController(controller: UIPresentationController) -> UIModalPresentationStyle
    {
        return UIModalPresentationStyle.None
    }
}