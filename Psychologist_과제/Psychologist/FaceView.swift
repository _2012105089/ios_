//
//  FaceView.swift
//  happiness
//
//  Created by Jo Sung Uk on 2016. 4. 5..
//  Copyright © 2016년 Jo Sung Uk. All rights reserved.
//

import UIKit

//프로토콜 선언
protocol FaceViewDataSource: class{
    func smilinessForFaceView(sender: FaceView) -> Double?
}

@IBDesignable
class FaceView: UIView {
    @IBInspectable
    var lineWidth: CGFloat = 4 { didSet { setNeedsDisplay() } }
    @IBInspectable
    var color: UIColor = UIColor.blueColor(){ didSet { setNeedsDisplay() } }
    @IBInspectable
    var scale: CGFloat = 0.90{ didSet { setNeedsDisplay() } }
    
    var faceCenter: CGPoint{
        return convertPoint(center, fromView: superview)
    }
    
    var faceRadius: CGFloat{
        return min(bounds.size.width, bounds.size.height) / 2 * scale
    }
    
    
    /*
     override func drawRect(rect: CGRect)
     {
     let facePath = UIBezierPath(arcCenter: faceCenter, radius: faceRadius, startAngle: 0, endAngle: CGFloat(2*M_PI), clockwise: true)
     
     facePath.lineWidth = lineWidth
     color.set()
     facePath.stroke()
     }
     */
    
    weak var dataSource: FaceViewDataSource? //프로토콜을 가지고 있는 친구다 라고 선언. 아웃렛을 설정할 때 컨트롤러가 ㅇㅇ안다
    
    func scale(gesture: UIPinchGestureRecognizer)
    {
        if gesture.state == .Changed {
            scale *= gesture.scale
            gesture.scale = 1
        }
    }
    
    
    private struct Scaling // 얼굴 요소들을 위한 고정 상수 모임(구조체: struct)
    {
        static let FaceRadiusToEyeRadiusRatio: CGFloat = 10
        static let FaceRadiusToEyeOffsetRatio: CGFloat = 3
        static let FaceRediusToEyeSeparationRatio: CGFloat = 1.5
        static let FaceRediusToMouthWidthRatio: CGFloat = 1
        static let FaceRediusToMouthHeighRatio: CGFloat = 3
        static let FaceRediusToMouthOffsetRatio: CGFloat = 3
    }
    private enum Eye { case Left, Right } // 눈 위치(왼, 오)
    private func bezierPathForEye(whichEye: Eye) -> UIBezierPath // 눈 함수
    {
        let eyeRadius = faceRadius / Scaling.FaceRadiusToEyeRadiusRatio
        let eyeVerticalOffset = faceRadius / Scaling.FaceRadiusToEyeOffsetRatio
        let eyeHorizontalSeparation = faceRadius / Scaling.FaceRediusToEyeSeparationRatio
        var eyeCenter = faceCenter
        eyeCenter.y -= eyeVerticalOffset
        switch whichEye
        {
        case .Left: eyeCenter.x -= eyeHorizontalSeparation / 2
        case .Right: eyeCenter.x += eyeHorizontalSeparation / 2
        }
        let path = UIBezierPath(arcCenter: eyeCenter, radius: eyeRadius, startAngle: 0, endAngle: CGFloat(2*M_PI),
                                clockwise: true)
        path.lineWidth = lineWidth
        return path
    }
    
    private func bezierPathForSmile(fractionOfMaxSmile: Double) -> UIBezierPath
    {
        let mouthWidth = faceRadius / Scaling.FaceRediusToMouthWidthRatio
        let mouthHeight = faceRadius / Scaling.FaceRediusToMouthHeighRatio
        let mouthVerticalOffset = faceRadius / Scaling.FaceRediusToMouthOffsetRatio
        let smileHeight = CGFloat(max(min(fractionOfMaxSmile, 1), -1)) * mouthHeight
        let start = CGPoint(x: faceCenter.x - mouthWidth / 2, y: faceCenter.y + mouthVerticalOffset)
        let end = CGPoint(x: start.x + mouthWidth, y: start.y)
        let cp1 = CGPoint(x: start.x + mouthWidth / 3, y: start.y + smileHeight)
        let cp2 = CGPoint(x: end.x - mouthWidth / 3, y: cp1.y)
        let path = UIBezierPath()
        path.moveToPoint(start)
        path.addCurveToPoint(end, controlPoint1: cp1, controlPoint2: cp2)
        path.lineWidth = lineWidth
        return path
    }
    
    override func drawRect(rect: CGRect)
    {
        let facePath = UIBezierPath(arcCenter: faceCenter, radius: faceRadius, startAngle: 0, endAngle: CGFloat(2*M_PI), clockwise: true)
        facePath.lineWidth = lineWidth
        color.set()
        facePath.stroke()
        
        bezierPathForEye(.Left).stroke()
        bezierPathForEye(.Right).stroke()
        
        let smiliness = dataSource?.smilinessForFaceView(self) ?? 0.0//?? -> 앞에가 닐이 아님 그대로 쓰고 닐이면 그대로 써라...
        let smilePath = bezierPathForSmile(smiliness)
        smilePath.stroke()
    }
    
}






















