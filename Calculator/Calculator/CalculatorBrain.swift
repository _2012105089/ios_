//
//  CalculatorBrain.swift
//  Calculator
//
//  Created by Jo Sung Uk on 2016. 3. 22..
//  Copyright © 2016년 COMP420. All rights reserved.
//

import Foundation

class CalculatorBrain{
    private enum Op : CustomStringConvertible{
        case Operand(Double)
        case UniaryOperation(String, Double -> Double)
        case BinaryOperation(String, (Double, Double) ->  Double)
    
        var description: String{
            get{
                switch self{
                case.Operand(let operand):
                    return "\(operand)"
                case.UniaryOperation(let symbol,_):
                    return symbol
                case.BinaryOperation(let symbol,_):
                    return symbol
                }
            }
            
        }
    }
    private var opStack = [Op]() // Array<Op> --> [Op]()

    private var KnownOps = [String: Op]() // key와 value로 되어있는 dictionary를 만든다.
    
    init(){
        func learnOp(op: Op){
            KnownOps[op.description] = op
        }
        learnOp(Op.BinaryOperation("✖️", *))
        learnOp(Op.BinaryOperation("➗") {$1 / $0})
        learnOp(Op.BinaryOperation("➕", +))
        learnOp(Op.BinaryOperation("➖") {$1 - $0})
        learnOp(Op.UniaryOperation("√" ,sqrt))
        learnOp(Op.UniaryOperation("sin"){sin($0)})
        learnOp(Op.UniaryOperation("cos"){cos($0)})
        
        /*KnownOps["✖️"] = Op.BinaryOperation("✖️", *)
        KnownOps["➗"] = Op.BinaryOperation("➗") {$1 / $0}
        KnownOps["➕"] = Op.BinaryOperation("➕", +)
        KnownOps["➖"] = Op.BinaryOperation("➖") {$1 - $0}
        KnownOps["√"] = Op.UniaryOperation("√" ,sqrt)*/
        
    }//simple init
    
    private func evaluate(ops: [Op]) -> (result: Double?,remainingOps: [Op]){
        if !ops.isEmpty{
            var remainingOps = ops
            let op = remainingOps.removeLast()//mutable & emutable & valuetype
            switch op{
            case .Operand(let operand):
                return (operand, remainingOps)
            case.UniaryOperation(_, let operation):
                let operandEvaluation = evaluate(remainingOps)//opetamdEvalution은 튜플이다.
                if let operand = operandEvaluation.result{
                    return(operation(operand), operandEvaluation.remainingOps)
                }
            case.BinaryOperation(_,let operation):
                let op1Evaluation = evaluate(remainingOps)//opetamdEvalution은 튜플이다.
                if let operand1 = op1Evaluation.result {
                    let op2Evaluation = evaluate(op1Evaluation.remainingOps)
                    if let operand2 = op2Evaluation.result {
                        return(operation(operand1, operand2), op2Evaluation.remainingOps)
                    }
                }
            }
        }
        return (nil, ops)
    }
    
    func evaluate() -> Double? {
        let(result,remainer) = evaluate(opStack)
        print("\(opStack) = \(result) with \(remainer) left over")
        return result
    }
    
    func pushOperand(operand: Double) -> Double?{
        opStack.append(Op.Operand(operand)) //discremite union
        return evaluate()
    }
    
    func performOperation(symbol: String) -> Double?{
        if let operation = KnownOps[symbol] {
            opStack.append(operation)
        }// null을 리턴 할 수 있기 때문에, 항상 optional인지 아닌지 체크를 해줘야한다.
        return evaluate()
    }
    
}