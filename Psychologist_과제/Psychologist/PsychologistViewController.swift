//
//  ViewController.swift
//  Psychologist
//
//  Created by Jo Sung Uk on 2016. 4. 26..
//  Copyright © 2016년 Jo Sung Uk. All rights reserved.
//

import UIKit

class PsychologistViewController: UIViewController {

    @IBAction func nothing(sender: AnyObject) {
        performSegueWithIdentifier("nothing", sender: nil)
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        var destination = segue.destinationViewController as UIViewController
        if let navCon = destination as? UINavigationController{
            destination = navCon.visibleViewController!
        }
        if let hvc = destination as? HappinessViewController{
            if let identifier = segue.identifier{
                switch identifier {
                    case "crying" : hvc.happiness = 10
                    case "smile" : hvc.happiness = 100
                    case "whatever" : hvc.happiness = 70
                    default : hvc.happiness = 50
                }
            }
        }
    }
    
    
}

