//
//  ViewController.swift
//  calulator2
//
//  Created by Jo Sung Uk on 2016. 3. 28..
//  Copyright © 2016년 Jo Sung Uk. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    
    @IBOutlet weak var display: UILabel!
    @IBOutlet weak var History: UILabel!
    
   
    var userInTheMiddleOfTypingNumber = false
    
    var brain = CalculatorBrain()
    
    var π = M_PI
    
    @IBAction func appendDigit(sender: UIButton) {
    
        let digit = sender.currentTitle!
        
        if userInTheMiddleOfTypingNumber {
            display.text = display.text! + digit
        } else {
            display.text = digit
            userInTheMiddleOfTypingNumber = true
        }
        
    }
    
    @IBAction func operate(sender: UIButton) {
    showHistory()
    //let operation = sender.currentTitle!
        if userInTheMiddleOfTypingNumber {
            enter()
        }
        if let operation = sender.currentTitle {
            if let result = brain.performOperation(operation) {
                displayValue = result
            } else {
                displayValue = 0
            }
        }
    }
    
    var displayValue: Double {
        get {
            return NSNumberFormatter().numberFromString(display.text!)!.doubleValue
        }
        set {
            display.text = "\(newValue)"
        }
    }
    @IBAction func pi(sender: AnyObject) {
        if userInTheMiddleOfTypingNumber {
            enter()
        }
        
        display.text = "π"
        
    }
    
    func showHistory(){
        History.text = brain.showHistory()
    }
    
    @IBAction func reset(sender: AnyObject) {
        userInTheMiddleOfTypingNumber = false
        display.text = "0"
        brain.reset()
        enter()
        showHistory()
    }
    
    @IBAction func remove(sender: AnyObject) {
        userInTheMiddleOfTypingNumber = false
        brain.remove()
        showHistory()
    }
    
    @IBAction func decimal(sender: AnyObject) {
        userInTheMiddleOfTypingNumber = true
        if display.text!.containsString(".") {
        
        }else{
            display.text = display.text! + "."
        }
    }
    
    @IBAction func Love(sender: AnyObject) {
        userInTheMiddleOfTypingNumber = false
        display.text = "0"
        brain.reset()
        enter()
        History.text = "정순기교수님 사랑합니다...😘"
        }
    
    @IBAction func enter() {
        userInTheMiddleOfTypingNumber = false
        if display.text != "π"{
            if let result = brain.pushOperand(displayValue) {
                displayValue = result
                showHistory()
            }else{
                displayValue = 0
                showHistory()
            }
        }
        else{
            display.text = "π"
            brain.pushOperand(M_PI)
        }
    }
}