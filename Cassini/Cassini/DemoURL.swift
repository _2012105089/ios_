//
//  Demo.swift
//  Cassini
//
//  Created by Jo Sung Uk on 2016. 5. 10..
//  Copyright © 2016년 Jo Sung Uk. All rights reserved.
//

import Foundation

struct DemoURL {
     //static let Stanford = NSURL(string: "https://comm.stanford.edu/wp-content/uploads/2013/01/stanford-compus.png")
    static let Coachella = NSURL(string: "https://coachella-2014-site.s3.amazonaws.com/wp-content/uploads/2016/01/07144549/daytime-balloons1a.jpg")
    static let JSU = NSURL(string: "https://static.wixstatic.com/media/e220eb_4a3e1f945368422a87431921471b0435.jpg/v1/fill/w_610,h_853,al_c,q_85/e220eb_4a3e1f945368422a87431921471b0435.jpg")
    struct NASA {
        static let Cassini = NSURL(string: "http://www.jpl.nasa.gov/images/cassini/20090202/pia03883-full.jpg")
        static let Earth = NSURL(string: "https://www.nasa.gov/sites/default/files/wave_earth_mosaic_3.jpg")
        static let Saturn = NSURL(string: "https://www.nasa.gov/sites/default/files/saturn_collage.jpg")
    }
}